#! /usr/bin/env python3

import os
from setuptools import setup, find_packages


here = os.path.abspath(os.path.dirname(__file__))


def read(fname):
    return open(os.path.join(here, fname)).read()


with open(os.path.join(here, "yaqd_MCP9600", "VERSION")) as version_file:
    version = version_file.read().strip()

extra_files = {"yaqd_MCP9600": ["VERSION"]}

setup(
    name="yaqd_MCP9600",
    packages=find_packages(),
    package_data=extra_files,
    python_requires=">=3.7",
    install_requires=["yaqd-core", "smbus"],
    extras_require={"dev": ["black", "pre-commit"]},
    version=version,
    description="yaq daemon for control of MCP9600 thermocouple sensor chip",
    author="yaq Developers",
    license="LGPL v3",
    url="https://gitlab.com/yaq/yaqd-mcp9600",
    entry_points={"console_scripts": ["yaqd-MCP9600=yaqd_MCP9600._MCP9600:MCP9600.main"]},
    keywords="yaq hardware temperature thermocouple",
    classifiers=[
        "Development Status :: 1 - Planning",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Topic :: Scientific/Engineering",
    ],
)
